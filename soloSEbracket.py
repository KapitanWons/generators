# coding: utf8
import math


class Person:
    def __init__(self, name, race, flag):
        self.name = name
        self.race = race
        self.flag = flag

    def print_info(self):
        print("name: %s, race: %s, flag: %s" % (self.name, self.race, self.flag))


class BracketGenerator:
    round_comment_tuple = (
        " <!-- ROUND OF 64 -->",
        " <!-- ROUND OF 32 -->",
        " <!-- ROUND OF 16 -->",
        " <!-- QUARTERFINALS -->",
        " <!-- SEMIFINALS -->",
        " <!-- FINAL MATCH -->"
    )

    third_place_round_comment = " <!-- 3RD PLACE (optional) -->"

    def __init__(self, player_list):
        self.player_list = player_list

    def simple_round_line_gen(self, round_count, player_no, player, is_third_place_match):
        player_values = [
            "=" + player.name,
            "race=" + player.race,
            "flag=" + player.flag,
            "score=",
            "win="
        ]
        player_line = "\n"
        for x in player_values:
            player_line += "|R{}{}{}{} ".format(round_count, "D" if round_count == 1 or is_third_place_match else "W", player_no, x)

        return player_line

    def generate_simple_round(self, round_count, players_per_round, players, maps_number):
        simple_round = ""
        is_third_place_match = 0
        if self.add_third and players_per_round == 1:
            players_per_round = 2
            is_third_place_match = 1
        for x in range(1, players_per_round + 1):
            simple_round += self.simple_round_line_gen(round_count, x, players[x - 1], is_third_place_match)
            if maps_number > 0 and x % 2 == 0:
                # 3rd place is G2 in final round
                simple_round += "\n|R%sG%sdetails = {{BracketMatchSummary\n" % (round_count, (str(int(x/2)+1)) if round_count > 1 else (str(int(x/2))) )
                simple_round += "|date=\n|vod=\n"
                for m in range(1, maps_number + 1):
                    simple_round += "|map{0}= |map{0}win= |vodgame{0}= |map{0}p1heroes= |map{0}p2heroes=\n".format(m)
                simple_round += "}} "

        return simple_round

    def generate_rounds(self, size, add_players, maps_number):
        bracket = ""
        players_per_round = size
        for count, value in enumerate([*range(int(math.log2(size) + self.add_third), 0, -1)], start=1):
            player_list = list([Person("", "", "")] * size)
            if add_players and count == 1:
                player_list = list(self.player_list)
                if len(player_list) < size:
                    temp_list = list([Person("", "", "")] * (size - len(player_list)))
                    player_list.extend(temp_list)
            #print(players_per_round)
            if self.add_third and players_per_round == 1:
                bracket += str(self.third_place_round_comment)
                value += 1
                count -= 1
            else:
                bracket += str(self.round_comment_tuple[-value + self.add_third])
            bracket += (self.generate_simple_round(count, players_per_round, player_list, maps_number))
            players_per_round = int(players_per_round / 2)
            bracket += "\n"

        return bracket

    def generate_bracket(self, size, add_third, add_players, maps_number=0):
        self.add_third = add_third
        return "{{%sSEBracket\n%s}}" % (
            str(size),
            self.generate_rounds(size, add_players, maps_number)
        )


if __name__ == "__main__":
    """
    List of players:
    Person("name", "race", "country"),
    Person("", "", ""),  is free slot
    """
    gen = BracketGenerator([
        Person("BriNGeR", "n", "fi"),
        Person("elfittaja", "u", "az"),
        Person("", "", ""),
        Person("Konna", "o", "cn"),
    ],
    )

    """
    size=8 => means 8SEBracket
    add_third=1, => Add third place match 1/0
    add_players=1, => Add players to the bracket 1/0
    maps_number=0 => maps in details
    """

    print(gen.generate_bracket(
        size=8,
        add_third=1,
        add_players=1,
        maps_number=0
    ))
