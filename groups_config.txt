[config]

# Add CrossTable: 1 - true; 0 - false
CrossTable=1
# Add Match List: 1 - true; 0 - false
AddMatchList=1

# Add Rounds/Playdays below table: 1 - true; 0 - false
AddPlayDays=0
# Change name of each Round/Playday
PlayDayName=Week
# Change the number of matches per round/playday
# 0 - default number: number of matches / number of players - 1
# It will give results: 4 players -> 2 per round, 5 -> 2, 6 -> 3
PlayDayPerRound=0

# Do not fill Match List with data (blank template): 1 - true; 0 - false
ClearMatchList=0

# Number of maps in Match List
BestOf=3

# Use custom match order for Match List (proper algorithm wanted to automatize!!!). Data is list of numbers
# List length must has the same length as number of matches. The formula is (n-1) * n/2 so for 4 -> 6, 5 -> 10, 6 -> 15 etc.
# There is default match order for 4 player group 1,6,2,5,3,4
# You can use debug option without matchOrder to see an order of matches and then sort numbers by your will (doubles are possible). Example at the bottom of file.
# MatchOrder=1,2,3,4,5,6
MatchOrder=0

# Shows debug message: 1 - true; 0 - false
Debug=0

#	Examples:
#
#	1. Using matchOrder Scenario with 5 players:
#
#	* use two options matchOrder = [], debug=True and execute
#	We should get something like this:
#	DEBUG INFO:
#
#	player list:
#	Name: Sky       Race: h         Flag: cn
#	Name: Grubby    Race: o         Flag: nl
#	Name: Moon      Race: n         Flag: kr
#	Name: FuRy      Race: u         Flag: se
#	Name: Axlav     Race: r         Flag: us
#	Sum: 5
#
#	Match  1:       Sky vs Grubby
#	Match  2:       Sky vs Moon
#	Match  3:       Sky vs FuRy
#	Match  4:       Sky vs Axlav
#	Match  5:    Grubby vs Moon
#	Match  6:    Grubby vs FuRy
#	Match  7:    Grubby vs Axlav
#	Match  8:      Moon vs FuRy
#	Match  9:      Moon vs Axlav
#	Match 10:      FuRy vs Axlav
#
#	It is not nice, but we will use those numbers in second step
#
#	* use two options matchOrder = [9, 10, 7, 8, 5, 6, 4, 3, 2, 1], debug=True and execute
#	And now we get something like this:
#
#	player list:
#	Name: Sky       Race: h         Flag: cn
#	Name: Grubby    Race: o         Flag: nl
#	Name: Moon      Race: n         Flag: kr
#	Name: FuRy      Race: u         Flag: se
#	Name: Axlav     Race: r         Flag: us
#	Sum: 5
#
#	Match  1:      Moon vs Axlav
#	Match  2:      FuRy vs Axlav
#	Match  3:    Grubby vs Axlav
#	Match  4:      Moon vs FuRy
#	Match  5:    Grubby vs Moon
#	Match  6:    Grubby vs FuRy
#	Match  7:       Sky vs Grubby
#	Match  8:       Sky vs Moon
#	Match  9:       Sky vs FuRy
#	Match 10:       Sky vs Axlav
#
#	* now you can this matchOrder list with debug=False and execute