import itertools
import math
#import configparser
#import yaml


class Person:
    def __init__(self, name, race, flag):
        self.name = name
        self.race = race
        self.flag = flag

    def print_info(self):
        print("name: %s, race: %s, flag: %s" % (self.name, self.race, self.flag))


class Generator:
    def __init__(self, list):
        self.list = list;

    def generate_crosst(self):
        table_begin = "{{CrossTable\n"
        table_teams = ""
        table_games = ""
        table_end = "}}\n"

        i = len(self.list)
        for x in range(i):
            table_teams += ("|team%s=%s\n" % (str(x + 1), self.list[x].name))
        for x in range(i - 1):
            y = x + 1
            table_games += ("<!--Games of Team %s-->\n" % (str(y)))
            while y <= i - 1:
                y += 1
                table_games += ("|%svs%sresult=|%svs%sresultvs=\n" % (str(x + 1), str(y), str(x + 1), str(y)))

        return ("%s%s%s%s" % (table_begin, table_teams, table_games, table_end))

    def generate_slots(self, bo=3):
        table_begin = "{{GroupTableStart| |width=|date=|mode=1v1}}\n"
        table_slots = ""
        table_end = "{{GroupTableEnd}}\n"

        for x in range(len(self.list)):
            table_slots += (
                    "{{GroupTableSlot|{{player|flag=%s|race=%s|%s}} |place= |win_m= |lose_m= %s|diff= |bg=}}\n" % (
                self.list[x].flag,
                self.list[x].race,
                self.list[x].name,
                "|win_g= |lose_g= " if bo > 1 else ""
            ))

        return ("%s%s%s" % (table_begin, table_slots, table_end))

    def generate_matchlist(self, addMatchList=False, addPlayDays=False, playDayName="Week", playDayPerRound=0,
                           clearMList=False, bo=3, matchOrder=[], debug=False):
        table_matchlist = ""
        playday_matchlist = ""

        player_list = self.list
        # Blank MatchList
        if clearMList:
            player_list = [Person("", "", "") for x in self.list]
        # All combinations
        pair_list = list(itertools.combinations(player_list, 2))
        # Match Order part
        number_of_matches = int(len(player_list) / 2.0 * (len(player_list) - 1))
        matches_per_day = math.floor(number_of_matches / int(len(player_list) - 1))
        if playDayPerRound > 0:
            matches_per_day = playDayPerRound
        match_order = list(range(1, len(pair_list) + 1))
        if len(player_list) == 4:
            match_order = [1, 6, 2, 5, 3, 4]
        if len(matchOrder) > 0:
            if len(matchOrder) != number_of_matches:
                return "\n\n!!!\n\nMatch Order List length is {} and number of matches for {} players is {}\n\n!!!\n\n".format(
                    len(matchOrder), len(player_list), number_of_matches)
            match_order = matchOrder

        if debug:
            debug_info = ""
            for idx, x in enumerate(match_order):
                pair = pair_list[x - 1]
                debug_info += "Match {:2d}:".format(int(idx + 1)) + "{:>10} vs {:<10}\n".format(pair[0].name,
                                                                                                pair[1].name)

            return debug_info
        if addMatchList:
            table_matchlist += "{{MatchList|hide=false\n"
            for idx, x in enumerate(match_order):
                pair = pair_list[x - 1]
                table_matchlist += ("|match%s={{MatchMaps\n" % str(idx + 1))
                table_matchlist += (
                        "|p1=%s |p1race=%s |p1flag=%s |p1score=\n" % (pair[0].name, pair[0].race, pair[0].flag))
                table_matchlist += (
                        "|p2=%s |p2race=%s |p2flag=%s |p2score=\n" % (pair[1].name, pair[1].race, pair[1].flag))
                table_matchlist += ("|winner= |details={{GroupMatchSummary\n")
                table_matchlist += ("|date=\n")
                table_matchlist += ("|vod=\n")
                for y in range(bo):
                    table_matchlist += ("|map{0}= |map{0}win= {1}|map{0}p1heroes= |map{0}p2heroes=\n".format(y + 1,
                                                                                                             "|vodgame{0}= ".format(
                                                                                                                 y + 1) if bo > 1 else ""))
                table_matchlist += ("}}}}\n")
            table_matchlist += "}}\n"

        if addPlayDays:
            match_order_index = 1
            playday_matchlist += "{{Box|end}}\n"
            playday_matchlist += "{{Toggle group start|width=}}\n"
            for x in range(1, int(number_of_matches / matches_per_day + 2)):
                playday_matchlist += "{{Box|break|padding=2em}}\n" if match_order_index > 1 else "{{Box|start|padding=2em}}\n"
                playday_matchlist += "{{MatchList|title=%s %s\n" % (playDayName, x)
                for y in range(matches_per_day):
                    try:
                        pair = pair_list[match_order[match_order_index - 1] - 1]
                        playday_matchlist += ("|match%s={{MatchMaps\n" % str(y + 1))
                        playday_matchlist += ("|p1=%s |p1race=%s |p1flag=%s |p1score=\n" % (
                            pair[0].name, pair[0].race, pair[0].flag))
                        playday_matchlist += ("|p2=%s |p2race=%s |p2flag=%s |p2score=\n" % (
                            pair[1].name, pair[1].race, pair[1].flag))
                        playday_matchlist += ("|winner= |details={{GroupMatchSummary\n")
                        playday_matchlist += ("|date=\n")
                        playday_matchlist += ("|vod=\n")
                        for z in range(bo):
                            playday_matchlist += (
                                "|map{0}= |map{0}win= {1}|map{0}p1heroes= |map{0}p2heroes=\n".format(z + 1,
                                                                                                     "|vodgame{0}= ".format(
                                                                                                         z + 1) if bo > 1 else ""))
                        playday_matchlist += ("}}}}\n")
                        match_order_index += 1
                    except:
                        variable = 'error'
                playday_matchlist += "}}\n"
            playday_matchlist += "{{Box|end}}\n"
            playday_matchlist += "{{Toggle group end}}\n"

        return table_matchlist + playday_matchlist

    def generate_group_table(self, crossT=True, addMatchList=True, addPlayDays=True, playDayName="Week",
                             playDayPerRound=0, clearMatchList=False, BO=3,
                             matchOrder=[], debug=False):
        box_break = "{{Box|break|padding=1em}}\n"

        if debug == True:
            player_list = "\n".join(
                ("Name: {:<10}Race: {:<10}Flag: {:<10}").format(x.name, x.race, x.flag) for x in self.list)
            return (
                    "DEBUG INFO:\n\n" +
                    "player list:\n" + player_list + "\nSum: {}\n\n".format(len(self.list)) +
                    self.generate_matchlist(matchOrder=matchOrder, debug=True)
            )

        return ("{{Box|start|padding=1em}}\n%s%s%s{{Box|end}}" % (
            (self.generate_crosst() + box_break) if crossT == True else "",
            self.generate_slots(BO) + box_break,
            self.generate_matchlist(addMatchList=addMatchList, addPlayDays=addPlayDays, playDayName=playDayName,
                                    playDayPerRound=playDayPerRound, clearMList=clearMatchList, bo=BO,
                                    matchOrder=matchOrder) if addMatchList == True or addPlayDays == True else "",
        ))

    def print_list(self):
        for x in self.list:
            print("name: %s, race: %s, flag: %s" % (x.name, x.race, x.flag))


if __name__ == '__main__':
    """
    How to use:
    add players to Generator as below:
    Person("NICKNAME", "RACE", "FLAG")
    
    Blank to copy:
    
    Person("", "", ""),
    
    Place of players in list determine Match List order
    """

    gen = Generator([
        Person("BriNGeR", "n", "fi"),
        Person("elfittaja", "u", "fi"),
        Person("Sein", "o", "fi"),
        Person("Konna", "o", "fi"),
        # Person("Axlav", "r", "by"),
        # Person("Infi", "m", "cn"),
    ])

    """
    Groups are generated by printing generate_group_table()
    fe. print(gen.generate_group_table(crossT=True, addMatchList=True, clearMatchList=False))
    
    Additional Options:
    crossT=         -> Add CrossTable:          True/False or 1/0 // Default True
    addMatchList=   -> Add Match List part:     True/False or 1/0 // Default True
    addPlayDays     -> Add Rounds/Playdays below table: True/False or 1/0 // Default False
    playDayName     -> Change name of each Round:   text string // Default "Week"
    playDayPerRound -> Change the number of matches per playday. // Default 0
                        Value 0 will use default method of counting, number of matches / number of players - 1.
                        It will give results: 4 players -> 2 per round, 5 -> 2, 6 -> 3 
    clearMatchList= -> Do not fill Match List with data (blank template):   True/Flase or 1/0 // Default False
    BO=             -> Number of maps in Match List:    number // Default 3
    matchOrder=     -> Use custom match order for Match List (proper algorithm wanted to automatize!!!). Data is list of numbers // Default blank list []
                        List length must has the same length as number of matches. The formula is (n-1) * n/2 so for 4 -> 6, 5 -> 10, 6 -> 15 etc.
                        There is default match order for 4 player group [1, 6, 2, 5, 3, 4]
                        You can use debug option without matchOrder to see an order of matches and then sort numbers by your will (doubles are possible). Example at the bottom of file.
    debug           -> Shows debug message:     True/Flase or 1/0 // Default False
    
    printing gen.generate_group_table() without additional options will use default values
    """

    print(gen.generate_group_table(
        crossT=True,
        addMatchList=True,
        addPlayDays=False,
        playDayName="Week",
        playDayPerRound=0,
        clearMatchList=False,
        BO=1,
        matchOrder=[],
        debug=False
    ))

    # PART FOR CONFIG FILES
    """
    parser = configparser.ConfigParser()
    parser.read("groups_config.txt")
    players = yaml.load(open("players.yaml"), Loader=yaml.FullLoader)
    
    list_of_players = []
    for key, value in players["players"].items():
        list_of_players.append(Person(key, str(value["race"]), str(value["flag"])))
    gen = Generator(
        list_of_players
    )

    output = gen.generate_group_table(
        crossT=int(parser.get("config", "CrossTable")),
        addMatchList=int(parser.get("config", "AddMatchList")),
        addPlayDays=int(parser.get("config", "AddPlayDays")),
        playDayName=str(parser.get("config", "PlayDayName")),
        playDayPerRound=int(parser.get("config", "PlayDayPerRound")),
        clearMatchList=int(parser.get("config", "ClearMatchList")),
        BO=int(parser.get("config", "BestOf")),
        matchOrder=[] if list(str(parser.get("config", "MatchOrder")).split(","))[0] == "0" else
        list(int(x) for x in str(parser.get("config", "MatchOrder")).split(",")),
        debug=int(parser.get("config", "Debug"))
    )

    print(output)
    with open('last_output', 'w') as f:
        print(output, file=f)

    input("Output was written to last_output file\nPress Enter to continue...")
    """

"""
Examples:

1. Using matchOrder Scenario with 5 players:

* use two options matchOrder = [], debug=True and execute
We should get something like this:
DEBUG INFO:

player list:
Name: Sky       Race: h         Flag: cn        
Name: Grubby    Race: o         Flag: nl        
Name: Moon      Race: n         Flag: kr        
Name: FuRy      Race: u         Flag: se        
Name: Axlav     Race: r         Flag: us        
Sum: 5

Match  1:       Sky vs Grubby    
Match  2:       Sky vs Moon      
Match  3:       Sky vs FuRy      
Match  4:       Sky vs Axlav     
Match  5:    Grubby vs Moon      
Match  6:    Grubby vs FuRy      
Match  7:    Grubby vs Axlav     
Match  8:      Moon vs FuRy      
Match  9:      Moon vs Axlav     
Match 10:      FuRy vs Axlav

It is not nice, but we will use those numbers in second step

* use two options matchOrder = [9, 10, 7, 8, 5, 6, 4, 3, 2, 1], debug=True and execute
And now we get something like this:

player list:
Name: Sky       Race: h         Flag: cn        
Name: Grubby    Race: o         Flag: nl        
Name: Moon      Race: n         Flag: kr        
Name: FuRy      Race: u         Flag: se        
Name: Axlav     Race: r         Flag: us        
Sum: 5

Match  1:      Moon vs Axlav     
Match  2:      FuRy vs Axlav     
Match  3:    Grubby vs Axlav     
Match  4:      Moon vs FuRy      
Match  5:    Grubby vs Moon      
Match  6:    Grubby vs FuRy      
Match  7:       Sky vs Grubby    
Match  8:       Sky vs Moon      
Match  9:       Sky vs FuRy      
Match 10:       Sky vs Axlav

* now you can this matchOrder list with debug=False and execute 
"""
